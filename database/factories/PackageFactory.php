<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Package;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Package::class, function (Faker $faker) {
    return [
        'code'  => Str::random(10),
        'id_cliente'    => function (){
            return factory(User::class)->create()->id;
        },
        'id_proveedor'  => function (){
            return factory(User::class)->create()->id;
        },
        'address'       => $faker->address,
    ];
});
