<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Auth;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class AuthTest
 * @package Tests\Feature\Api\Auth
 * @testdox Pruebas del modulo de Autenticacion de usuario
 */
class AuthTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @test
     * @return void
     * @testdox Registar usuario con datos correctos
     */
    public function register_user_with_correct_data()
    {
//        $this->withoutExceptionHandling();
        $data = [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
            'password'      => '12345678',
            'password_confirmation'    => '12345678',
        ];

        $response = $this->post('v1/api/auth/register', $data)
            ->getOriginalContent();
        $this::assertTrue($response['ok']);
        $this::assertIsString($response['access_token']);
        $this->assertDatabaseHas('users', [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ]);
    }

    /**
     * @test
     * @return void
     * @testdox Trata de registrar usuario con datos vacios (No registra)
     */
    public function register_user_without_data()
    {
        $data = [
            'first_name' => '',
            'last_name' => '',
            'role' => '',
            'email' => '',
            'password' => '',
            'password_confirmation' => '',
        ];

        $this->post('v1/api/auth/register', $data, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->assertSessionHasErrors([
                'first_name'    => "El campo nombre es obligatorio.",
                'last_name'     => "El campo apellido es obligatorio.",
                'email'         => "El campo email es obligatorio.",
                'role'          => "El campo role es obligatorio.",
                'password'      => "El campo password es obligatorio.",
                'password_confirmation' => 'La confirmacion es obligatoria',
            ]);

    }

    /**
     * @test
     * @return void
     * @testdox Trata de registrar usuario con datos malos (NO Registra)
     */
    public function register_user_with_bad_data()
    {
        $data = [
            'first_name' => '123457',
            'last_name' => '234567',
            'role' => 'asdad',
            'email' => 'iuytre',
        ];

        $this->post('v1/api/auth/register', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->assertSessionHasErrors([
                'first_name'    => "El formato del campo nombre es inválido.",
                'last_name'     => "El formato del campo apellido es inválido.",
                'role'          => "El campo role es inválido.",
                'email'         =>"El campo email debe ser una dirección de correo válida.",
            ]);

    }

    /**
     * @test
     * @return void
     * @testdox Trata de registrar usuario con password y password_confirmation diferentes (NO Registra)
     */
    public function register_user_with_password_and_anfirmation_diferent()
    {
        $data = [
            'password' => '12345678',
            'passowrd_confirmation' => '87654321',
        ];

        $this->post('v1/api/auth/register', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->assertSessionHasErrors([
                'password' => 'La confirmacion y el password no coinciden',
            ]);

    }

    /**
     * @test
     * @return void
     * @testdox Trata de registrar usuario con datos minimos en entradas (NO Registra)
     */
    public function register_user_with_minimus_data()
    {
        $data = [
            'first_name'    => 'aa',
            'last_name'     => 'aa',
            'email'         => 'a@d.c',
            'password'      => '12345'
        ];

        $this->post('v1/api/auth/register', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->assertSessionHasErrors([
                'first_name'    => "El campo nombre debe contener al menos 3 caracteres.",
                'last_name'     => "El campo apellido debe contener al menos 3 caracteres.",
                'email'         => "El campo email debe contener al menos 6 caracteres.",
                'password'      => "El campo password debe contener al menos 6 caracteres.",
            ]);

    }

    /**
     * @test
     * @return void
     * @testdox Trata de registrar usuario con datos maximos en entradas (NO Registra)
     */
    public function register_user_with_maximus_data()
    {
        $data = [
            'first_name'    => 'aafsasdadasdadasdadadasdadasdasdsdsdsad',
            'last_name'     => 'asdasdasdasdasdasdasdasdadasdasdasdasdas',
            'email'         => 'sdfdhdgdgdg@fdsfsdfsdfsdfsfsdfsfsdfsfs.cdf',
            'password'      => 'asdsdsdadasdadadadadasdadasdasdadasadadada'
        ];

        $this->post('v1/api/auth/register', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->assertSessionHasErrors([
                'first_name'    => "El campo nombre no debe contener más de 25 caracteres.",
                'last_name'     => "El campo apellido no debe contener más de 25 caracteres.",
                'email'         => "El campo email no debe contener más de 30 caracteres.",
                'password'      => "El campo password no debe contener más de 25 caracteres.",
            ]);

    }

    /**
     * @test
     * @return void
     * @testdox Login de usuario con datos correctos
     */
    public function login_user_with_correct_data()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['password' => bcrypt('12345678')]);

        $data = [
            'email'         => $user->email,
            'password'      => '12345678'
        ];

        $response = $this->post('v1/api/auth/login', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->getOriginalContent();

        $this::assertTrue($response['ok']);
        $this::assertIsString($response['access_token']);
        $this::assertInstanceOf(User::class, $response['user']);
        $this::assertTrue(Auth::check());


    }

    /**
     * @test
     * @return void
     * @testdox Login de usuario con passowrd diferente (No loguea)
     */
    public function login_user_with_password_diferent()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['password' => bcrypt('12345678')]);

        $data = [
            'email'         => $user->email,
            'password'      => '87654321'
        ];

        $response = $this->post('v1/api/auth/login', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->assertStatus(401)
            ->getOriginalContent();

        $this::assertFalse($response['ok']);
        $this::assertFalse(Auth::check());
        $this::assertEquals('Credenciales invalidas', $response['message']);

    }

    /**
     * @test
     * @return void
     * @testdox Login de usuario con email diferente (No loguea)
     */
    public function login_user_with_email_diferent()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create([
            'email'     => 'contacto@lauchoit.com',
            'password'  => bcrypt('12345678')
        ]);

        $data = [
            'email'         => 'jlaucho@gmail.com',
            'password'      => '12345678'
        ];

        $response = $this->post('v1/api/auth/login', $data, ['X-Requested-With' => 'XMLHttpRequest'])
            ->assertStatus(401)
            ->getOriginalContent();

        $this::assertFalse($response['ok']);
        $this::assertFalse(Auth::check());
        $this::assertEquals('Credenciales invalidas', $response['message']);

    }

    /**
     * @test
     * @return void
     * @testdox Actualizar usuario con datos correctos
     */
    public function update_user_with_correct_data()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['password' => bcrypt('12345678')]);
        $token = $user->createToken('authToken')->accessToken;

        $data = [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ];

        $headers = [
            'X-Requested-With' => 'XMLHttpRequest',
            'Accept' => 'application/json',
            'Authorization' => "Bearer {$token}",
        ];

        $response = $this->post("v1/api/auth/update", $data, $headers)
            ->assertStatus(200);
        $this::assertTrue($response['ok']);
        $this::assertEquals('Usuario actualizado correctamente', $response['message']);

        $this->assertDatabaseHas('users', [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ]);
    }

    /**
     * @test
     * @return void
     * @testdox Actualizar usuario con token invalido (No actualiza)
     */
    public function update_user_with_invalid_token()
    {
        $user = factory(User::class)->create(['password' => bcrypt('12345678')]);
        $token = $user->createToken('authToken')->accessToken;

        $data = [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ];

        $headers = [
            'X-Requested-With' => 'XMLHttpRequest',
            'Accept' => 'application/json',
            'Authorization' => "Bearer aaaaaaaaaa",
        ];

        $response = $this->post("v1/api/auth/update", $data, $headers)
            ->assertStatus(401)
            ->getOriginalContent();

        $this::assertEquals('Unauthenticated.', $response['message']);

        $this->assertDatabaseMissing('users', [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ]);
    }

    /**
     * @test
     * @return void
     * @testdox Actualizar usuario con campor vacios (No actualiza)
     */
    public function update_user_without_data()
    {
        $user = factory(User::class)->create(['password' => bcrypt('12345678')]);
        $token = $user->createToken('authToken')->accessToken;

        $data = [
            'first_name'    => '',
            'last_name'     => '',
            'role'          => '',
            'email'         => '',
        ];

        $headers = [
            'X-Requested-With' => 'XMLHttpRequest',
            'Authorization' => "Bearer {$token}",
        ];

        $this->post("v1/api/auth/update", $data, $headers)
            ->assertSessionHasErrors([
                'first_name'    => "El campo nombre es obligatorio.",
                'last_name'     => "El campo apellido es obligatorio.",
                'email'         => "El campo email es obligatorio.",
                'role'          => "El campo role es obligatorio.",
            ]);

        $this->assertDatabaseMissing('users', [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ]);
    }

    /**
     * @test
     * @return void
     * @testdox Actualizar usuario a un email duplicado (No actualiza)
     */
    public function update_user_email_duplicate()
    {
//        $this->withoutExceptionHandling();
        $originalUser = factory(User::class)->create(['email' => 'contacto@lauchoit.com']);

        $user = factory(User::class)->create([
            'email'     => 'jlaucho@gmail.com.com',
            'password'  => bcrypt('12345678')
        ]);
        $token = $user->createToken('authToken')->accessToken;

        $data = [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ];

        $headers = [
            'X-Requested-With' => 'XMLHttpRequest',
            'Authorization' => "Bearer {$token}",
        ];

        $this->post("v1/api/auth/update", $data, $headers)
            ->assertSessionHasErrors([
                'email' => "El valor del campo email ya está en uso.",
            ]);

        $this->assertDatabaseMissing('users', [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ]);
    }

    /**
     * @test
     * @return void
     * @testdox Actualizar usuario a nuevos datos manteniendo el mismo email (Actualiza)
     */
    public function update_user_with_self_email()
    {
        $user = factory(User::class)->create([
            'email'     => 'contacto@lauchoit.com',
            'password'  => bcrypt('12345678')
        ]);
        $token = $user->createToken('authToken')->accessToken;

        $data = [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ];

        $headers = [
            'X-Requested-With' => 'XMLHttpRequest',
            'Authorization' => "Bearer {$token}",
        ];

        $this->post("v1/api/auth/update", $data, $headers)
            ->assertStatus(200)
            ->getOriginalContent();

        $this->assertDatabaseHas('users', [
            'first_name'    => 'Jesus',
            'last_name'     => 'Laucho',
            'role'          => 'cliente',
            'email'         => 'contacto@lauchoit.com',
        ]);
    }
}
