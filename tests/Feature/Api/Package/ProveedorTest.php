<?php

namespace Tests\Feature\Api\Package;

use App\Models\Package;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class ProveedorTest
 * @package Tests\Feature\Api\Package
 * @testdox Modulo de pruebas a los proveedores
 */
class ProveedorTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @return void
     * @test
     * @testdox Usuarios no autenticados NO pueden acceder al metodo
     */
    public function cant_storage_package_user_guest()
    {
        $proveedor  = factory(User::class)->create(['role' => 'proveedor']);
        $cliente    = factory(User::class)->create(['role' => 'cliente']);
        $data = [
            'id_proveedor'  => $proveedor->id,
            'id_cliente'    => $cliente->id,
            'address'       => 'Direccion de destino'
        ];
        $this->post('v1/api/package/store', $data, ['Accept' => 'application/json'])
            ->assertStatus(401);

        $this->assertDatabaseMissing('packages', [
            'id_proveedor'  => $proveedor->id,
            'id_cliente'    => $cliente->id,
            'address'       => 'Direccion de destino'
        ]);
    }

    /**
     * @return void
     * @test
     * @testdox Registrar paquetes con los datos correctos
     */
    public function storage_package_from_proveedor()
    {
        $this->withoutExceptionHandling();
        $proveedor  = factory(User::class)->create(['role' => 'proveedor']);
        $cliente    = factory(User::class)->create(['role' => 'cliente']);
        $user       = factory(User::class)->create(['role' => 'proveedor']);
        $token      = $user->createToken('accessToken')->accessToken;

        $headers    = [
            'Authorization' => "Bearer {$token}",
            'Accept' => 'application/json',
        ];
        $data = [
            'id_proveedor'  => $proveedor->id,
            'id_cliente'    => $cliente->id,
            'address'       => 'Direccion de destino'
        ];
        $response = $this->post('v1/api/package/store', $data, $headers)
            ->assertStatus(201)
            ->getOriginalContent();

        $this->assertTrue($response['ok']);
        $this->assertInstanceOf(Package::class, $response['data']);

        $this->assertDatabaseHas('packages', [
            'id_proveedor'  => $proveedor->id,
            'id_cliente'    => $cliente->id,
            'address'       => 'Direccion de destino'
        ]);
    }

    /**
     * @return void
     * @test
     * @testdox Registrar paquetes con los campos vacios (No registra)
     */
    public function storage_package_from_proveedor_whithout_data()
    {
        $proveedor  = factory(User::class)->create(['role' => 'proveedor']);
        $cliente    = factory(User::class)->create(['role' => 'cliente']);
        $user       = factory(User::class)->create(['role' => 'proveedor']);
        $token      = $user->createToken('accessToken')->accessToken;

        $headers    = [
            'Authorization' => "Bearer {$token}",
        ];
        $data = [
            'id_proveedor'  => '',
            'id_cliente'    => '',
            'address'       => ''
        ];
        $response = $this->post('v1/api/package/store', $data, $headers)
            ->assertSessionHasErrors([
                'id_proveedor'  => "El campo de proveedor es obligatorio.",
                'id_cliente'    => "El campo de cliente es obligatorio.",
                'address'       => "El campo de direccion es obligatorio."
            ])
            ->getOriginalContent();

        $this::assertEquals(0, Package::count());
    }

    /**
     * @return void
     * @test
     * @testdox Registrar paquetes con los campos malos (No registra)
     */
    public function storage_package_from_proveedor_whit_bad_data()
    {
        $proveedor  = factory(User::class)->create(['role' => 'proveedor']);
        $cliente    = factory(User::class)->create(['role' => 'cliente']);
        $user       = factory(User::class)->create(['role' => 'proveedor']);
        $token      = $user->createToken('accessToken')->accessToken;

        $headers    = [
            'Authorization' => "Bearer {$token}",
        ];
        $data = [
            'id_proveedor'  => '124354',
            'id_cliente'    => '234565',
        ];
        $response = $this->post('v1/api/package/store', $data, $headers)
            ->assertSessionHasErrors([
                'id_proveedor'  => "El campo de proveedor seleccionado no existe.",
                'id_cliente'    => "El campo de cliente seleccionado no existe.",
            ])
            ->getOriginalContent();

        $this::assertEquals(0, Package::count());
    }

    /**
     * @return void
     * @test
     * @testdox Registrar paquetes con los campos minimos (No registra)
     */
    public function storage_package_from_proveedor_whit_minimus_data()
    {
        $proveedor  = factory(User::class)->create(['role' => 'proveedor']);
        $cliente    = factory(User::class)->create(['role' => 'cliente']);
        $user       = factory(User::class)->create(['role' => 'proveedor']);
        $token      = $user->createToken('accessToken')->accessToken;

        $headers    = [
            'Authorization' => "Bearer {$token}",
        ];
        $data = [
            'address'  => '12435',
        ];
        $this->post('v1/api/package/store', $data, $headers)
            ->assertSessionHasErrors([
                'address'  => "El campo de direccion debe contener al menos 6 caracteres.",
            ])
            ->getOriginalContent();

        $this::assertEquals(0, Package::count());
    }

    /**
     * @return void
     * @test
     * @testdox Registrar paquetes con los campos maximos (No registra)
     */
    public function storage_package_from_proveedor_whit_maximus_data()
    {
        $proveedor  = factory(User::class)->create(['role' => 'proveedor']);
        $cliente    = factory(User::class)->create(['role' => 'cliente']);
        $user       = factory(User::class)->create(['role' => 'proveedor']);
        $token      = $user->createToken('accessToken')->accessToken;

        $headers    = [
            'Authorization' => "Bearer {$token}",
        ];
        $data = [
            'address'  => '124sfsdfsfsfsdf sdf sdf sd fsd f sdfsdfsdfsdgsgsg sg d f  sg ers te a et ser terr t ert ete twetertewrte 35 efs fs fs df sf sd f f e ter t e  f sf s fs dfsd f s fsf s fs f sf sf sd fds fsd f sd f sdf s f sf sddf s fs df sd fsd f s f sdf sfsdfsfsdf s fsd f sdf',
        ];
        $this->post('v1/api/package/store', $data, $headers)
            ->assertSessionHasErrors([
                'address'  => "El campo de direccion no debe contener más de 250 caracteres.",
            ])
            ->getOriginalContent();

        $this::assertEquals(0, Package::count());
    }
}


