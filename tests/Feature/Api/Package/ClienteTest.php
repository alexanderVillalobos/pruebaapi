<?php

namespace Tests\Feature\Api\Package;

use App\Models\Package;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

/**
 * Class AlmacenTest
 * @package Tests\Feature\Api\Package
 * @testdox Modulo de pruebas al cliente
 */
class ClienteTest extends TestCase
{
    use DatabaseTransactions;
    /**
     *
     * @return void
     * @test
     * @testdox Condultar un paquete en cualquier momento
     */
    public function see_package_all_time()
    {
        $this->withoutExceptionHandling();
        $userClient = factory(User::class)->create(['role' => 'cliente']);
        $token  = $userClient->createToken('createToken')->accessToken;

        $packages_self  = factory(Package::class, 3)->create(['id_cliente' => $userClient->id]);
        $packages_others = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];

        $response = $this->get("v1/api/package/{$packages_self->first()->id}", $headers)
            ->getOriginalContent();
        $this->assertEquals('Su paquete esta en espera de envio', $response['data'] );
    }

    /**
     *
     * @return void
     * @test
     * @testdox Condultar un paquete que solo le pertenezca a el
     */
    public function see_package_time_only_self()
    {
        $this->withoutExceptionHandling();
        $userClient = factory(User::class)->create(['role' => 'cliente']);
        $token  = $userClient->createToken('createToken')->accessToken;

        $packages_self  = factory(Package::class, 3)->create();
        $packages_others = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];

        $response = $this->get("v1/api/package/{$packages_others->first()->id}", $headers)
            ->getOriginalContent();

        $this->assertEquals('El paquete seleccionado no existe en el almacen', $response['data']);
    }

    /**
     *
     * @return void
     * @test
     * @testdox Respuestas de los paquetes enviados consultados por el cliente
     */
    public function response_package_sent_fron_client()
    {
        $this->withoutExceptionHandling();
        $userClient = factory(User::class)->create(['role' => 'cliente']);
        $token  = $userClient->createToken('createToken')->accessToken;

        $packages_self  = factory(Package::class)->create();
        $packages_others = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];

        $response = $this->get("v1/api/package/65432", $headers)
            ->getOriginalContent();

        $this::assertFalse($response['ok']);
        $this::assertEquals('El paquete seleccionado no existe en el almacen', $response['data']);
    }

    /**
     *
     * @return void
     * @test
     * @testdox Respuestas de los paquetes enviados consultados exista pero no se ha aprobado
     */
    public function response_package_exist_not_aproved()
    {
        $this->withoutExceptionHandling();
        $userClient = factory(User::class)->create(['role' => 'cliente']);
        $token  = $userClient->createToken('createToken')->accessToken;

        $packages_self  = factory(Package::class)->create(['id_cliente'=> $userClient->id]);
        $packages_others = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];

        $response = $this->get("v1/api/package/{$packages_self->first()->id}", $headers)
            ->getOriginalContent();

        $this::assertTrue($response['ok']);
        $this::assertEquals('Su paquete esta en espera de envio', $response['data']);
    }

    /**
     *
     * @return void
     * @test
     * @testdox Respuestas de los paquetes enviados consultados exista y que se halla enviado
     */
    public function response_package_exist_and_sent()
    {
        $this->withoutExceptionHandling();
        $userClient = factory(User::class)->create(['role' => 'cliente']);
        $token  = $userClient->createToken('createToken')->accessToken;

        $packages_self1  = factory(Package::class)->create(['id_cliente'=> $userClient->id]);
        $packages_self2  = factory(Package::class)->create([
            'id_cliente'=> $userClient->id,
            'sent'      => 1,
            ]);
        $packages_others = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];

        $response = $this->get("v1/api/package/{$packages_self2->id}", $headers)
            ->getOriginalContent();

        $this::assertTrue($response['ok']);
        $this::assertEquals('Su paquete se ha enviado al destino', $response['data']);
    }

    /**
     *
     * @return void
     * @test
     * @testdox Marcar paquete como entregado por el cliente
     */
    public function package_sender_from_client()
    {
        $this->withoutExceptionHandling();
        $userClient = factory(User::class)->create(['role' => 'cliente']);
        $token  = $userClient->createToken('createToken')->accessToken;

        $packages_self1  = factory(Package::class)->create([
            'id_cliente'=> $userClient->id,
            'sent'      => 1
        ]);
        $packages_self2  = factory(Package::class)->create([
            'id_cliente'=> $userClient->id,
            'sent'      => 0,
        ]);

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];

        $data = [
            'delivered' => 1
        ];

        $response = $this->put("v1/api/package/delivered/{$packages_self1->id}",$data,  $headers)
            ->getOriginalContent();

        $this::assertTrue($response['ok']);
        $this::assertEquals('Su paquete se ha entregado', $response['data']);
    }
}
