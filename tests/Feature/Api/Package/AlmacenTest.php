<?php

namespace Tests\Feature\Api\Package;

use App\Models\Package;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

/**
 * Class AlmacenTest
 * @package Tests\Feature\Api\Package
 * @testdox Modulo de pruebas al almacen
 */
class AlmacenTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * @return void
     * @test
     * @testdox Consultar todos los paquetes
     */
    public function can_see_all_package()
    {
        $this->withoutExceptionHandling();
        $userAlmacen = factory(User::class)->create(['role' => 'almacen']);
        $token  = $userAlmacen->createToken('createToken')->accessToken;

        factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
            'Accept'        => 'application/json'
        ];

        $response = $this->get('v1/api/almacen', $headers)
            ->getOriginalContent();
        $this->assertTrue($response['ok']);
        $this->assertInstanceOf(LengthAwarePaginator::class, $response['data']);
        $this->assertInstanceOf(Package::class, $response['data']->first());

    }

    /**
     * @return void
     * @test
     * @testdox Aprobar el envio de uno de los paquetes (Actualiza)
     */
    public function send_package_aproved()
    {
        $this->withoutExceptionHandling();
        $userAlmacen = factory(User::class)->create(['role' => 'almacen']);
        $token  = $userAlmacen->createToken('createToken')->accessToken;

        $packages = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
            'Accept'        => 'application/json'
        ];
        $data = [
            'sent'  => '1'
        ];

        $response = $this->put("v1/api/almacen/{$packages->first()->id}",$data, $headers)
            ->getOriginalContent();

        $this->assertTrue($response['ok']);
        $this->assertInstanceOf(Package::class, $response['data']);

        $this->assertDatabaseHas('packages', [
            'id'    => $packages->first()->id,
            'sent'  => 1,
        ]);

    }

    /**
     * @return void
     * @test
     * @testdox Aprobar el envio de uno de los paquetes con datos malos (No actualiza)
     */
    public function send_package_bad_data()
    {
        $userAlmacen = factory(User::class)->create(['role' => 'almacen']);
        $token  = $userAlmacen->createToken('createToken')->accessToken;

        $packages = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];
        $data = [
            'sent'  => 'algo'
        ];

        $response = $this->put("v1/api/almacen/{$packages->first()->id}",$data, $headers)
            ->assertSessionHasErrors([
                'sent'  => "El campo sent debe ser verdadero o falso.",
            ])
            ->getOriginalContent();

    }

    /**
     * @return void
     * @test
     * @testdox Aprobar el envio de uno de los paquetes con datos malos del id (No actualiza)
     */
    public function send_package_bad_id_data()
    {
        $this->withoutExceptionHandling();
        $userAlmacen = factory(User::class)->create(['role' => 'almacen']);
        $token  = $userAlmacen->createToken('createToken')->accessToken;

        $packages = factory(Package::class, 20)->create();

        $headers = [
            'Authorization' => "Bearer {$token}",
        ];
        $data = [
            'sent'  => '1'
        ];

        $response = $this->put("v1/api/almacen/1234543",$data, $headers)
            ->getOriginalContent();
        $this::assertFalse($response['ok']);
        $this::assertEquals('Verifique el paquete seleccionado', $response['data']);
    }
}
