<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'auth', 'namespace' => 'Api\\'], function(){
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('update', 'AuthController@update')->middleware('auth:api');
    Route::get('me', 'AuthController@logued')->middleware('auth:api');
    Route::get('check', 'AuthController@checkToken')->middleware('auth:api');
});
Route::group(['prefix' => 'package', 'middleware' => 'auth:api', 'namespace' => 'Api\\'], function(){
    Route::post('store', 'PackageController@store');
    Route::get('/{package}', 'PackageController@find');
    Route::put('/delivered/{package}', 'PackageController@delivered');
});
Route::group(['prefix' => 'almacen', 'middleware' => 'auth:api', 'namespace' => 'Api\\'], function(){
    Route::get('', 'PackageController@all');
    Route::put('/{package}', 'PackageController@sent');
});
