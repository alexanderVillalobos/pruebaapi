<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $primaryKey   = 'id';
    protected $fillable     = [
        'id_proveedor', 'id_cliente', 'address',
    ];
}
