<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RequestUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name'    => 'required|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ.\s]+$/i|max:25|min:3',
            'last_name'     => 'required|regex:/^[a-zA-ZñÑáéíóúÁÉÍÓÚ.\s]+$/i|max:25|min:3',
            'email'         => ['required','email',Rule::unique('users', 'email')->ignore(auth()->id()),'min:6','max:30'],
            'role'          => 'required|in:proveedor,almacen,cliente',

        ];
        if( strpos( request()->fullUrl(), 'register') ) {
            $rules = array_merge($rules, [
                'password'              => 'required|confirmed|min:6|max:25',
                'password_confirmation' => 'required',
                'email'         => 'required|email|unique:users|min:6|max:30',
            ]);
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'password_confirmation.required' => 'La confirmacion es obligatoria',
            'password.confirmed' => 'La confirmacion y el password no coinciden',
        ];
    }

    public function attributes()
    {
        return [
            'first_name'    => 'nombre',
            'last_name'     => 'apellido',
        ];
    }
}
