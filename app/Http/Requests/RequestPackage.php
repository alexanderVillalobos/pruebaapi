<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestPackage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_cliente'    => 'required|exists:users,id',
            'id_proveedor'  => 'required|exists:users,id',
            'address'       => 'required|min:6|max:250',
        ];
    }

    public function attributes()
    {
        return [
            'id_cliente'    => 'de cliente',
            'id_proveedor'  => 'de proveedor',
            'address'       => 'de direccion',
        ];
    }
}
