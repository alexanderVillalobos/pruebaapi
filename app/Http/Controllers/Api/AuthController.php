<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestLogin;
use App\Http\Requests\RequestUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(RequestUser $request)
    {
        $user               = new User();
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->role         = $request->role;
        $user->email        = $request->email;
        $user->password     = bcrypt($request->password);
        $user->save();

        $accessToken = $user->createToken('authToken')->accessToken;

        return response()->json([
            'ok'    => true,
            'user'  => $user,
            'access_token'  => $accessToken
        ], 201);
    }

    public function login (RequestLogin $request)
    {
        $data = $request->only('email', 'password');

        if(!Auth::attempt($data)) {
            return response()->json([
                'ok'   =>false,
                'message'   => 'Credenciales invalidas'
            ], 401);
        }
        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response()->json([
            'ok'    => true,
            'user'  => auth()->user(),
            'access_token'  => $accessToken
        ],200);
    }

    public function update(RequestUser $request){
        $user = auth()->user();
        $user->first_name   = $request->first_name;
        $user->last_name    = $request->last_name;
        $user->email        = $request->email;
        $user->role         = $request->role;
        $user->save();

        return response()->json([
            'ok'        => true,
            'message'   => 'Usuario actualizado correctamente',
        ]);
    }

    public function logued()
    {
        return response()->json([
            'ok'    => true,
            'user'  => auth()->user()
        ]);
    }

    public function checkToken() {
        return response()->json([
            'ok'    => true
        ]);
    }
}
