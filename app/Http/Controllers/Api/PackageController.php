<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RequestPackage;
use App\Http\Requests\RequestSent;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PackageController extends Controller
{
    public function store(RequestPackage $request) {
        do {
            $code   = Str::random(10);
            $exist  = Package::where('code', $code)->first();
        } while ($exist);

        $package                = new Package();
        $package->code          = $code;
        $package->id_cliente    = $request->id_cliente;
        $package->id_proveedor  = $request->id_proveedor;
        $package->address       = $request->address;

        $package->save();

        return response()->json([
            'ok'    => true,
            'data'  => $package
        ], 201);
    }

    public function all(){
        $data = Package::select('*')
            ->paginate();
        return response()->json([
            'ok'    => true,
            'data'  => $data
        ]);
    }

    public function sent($package, RequestSent $request){
        $package = Package::find($package);

        if(!$package){
            return response()->json([
                'ok'    => false,
                'data'  => 'Verifique el paquete seleccionado'
            ], 400);
        }

        $package->sent = $request->sent;
        $package->save();

        return response()->json([
            'ok'    => true,
            'data'  => $package
        ]);
    }

    public function find($package){
        $package = Package::where([['id', $package], ['id_cliente', auth()->id()]])->first();

        if(!$package) {
            return response()->json([
                'ok'    => false,
                'data'  => 'El paquete seleccionado no existe en el almacen'
            ], 400);
        }
        if (!$package->sent) {
            return response()->json([
                'ok'    => true,
                'data'  => 'Su paquete esta en espera de envio'
            ], 200);
        }
        if ($package->sent) {
            return response()->json([
                'ok'    => true,
                'data'  => 'Su paquete se ha enviado al destino'
            ], 200);
        }
        return response()->json([
            'ok'    => true,
            'data'  => $package
        ]);
    }

    public function delivered ($package, Request $request){
        $package = Package::where([['id', $package], ['id_cliente', auth()->id()]])->first();
        if(!$package) {
            return response()->json([
                'ok'    => false,
                'data'  => 'El paquete seleccionado no existe en el almacen'
            ], 400);
        }
        $package->delivered = 1;
        $package->save();

        return response()->json([
            'ok'    => true,
            'data'  => 'Su paquete se ha entregado'
        ]);
    }
}
